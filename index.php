﻿
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
 

<form class="form" id="formSubmit">
    <div class="form-row">
            <label for="inputTitle">Title</label>
            <input type="text" id="inputTitle">
    </div>
    <div class="form-row">
            <label for="inpBody">Description of book</label>
            <textarea id="inpBody"></textarea>
    </div>
    <div class="form-row">
            <button type="submit" class="button">Send data</button>
    </div>
</form>

<script>

// use API to simulate sending data
const apiUrl = "https://jsonplaceholder.typicode.com";

// parameters from form 
const $form = $('.form');
const $inputTitle = $('#inpTitle');
const $inputBody = $('#inpBody');
const $submitBtn = $form.find(":submit");

// submit data in json with method POST
$form.on("submit", function(e) {
    e.preventDefault();

    $.ajax({
        url: apiUrl + '/posts',
        method : "POST",
        dataType : "json",
        data : {
            bookId : 1, 
            title : $inputTitle.val(), 
            body : $inputBody.val()
        }
    })
    .done(function(res) {
        console.log("Correct sending data", res);
    })
  
});
</script>